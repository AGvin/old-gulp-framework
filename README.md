# Gulp Micro Framework

*Small framework for better structuring gulp tasks, watches, events*


## Installation:

```
#!bash
npm i --save git+https://bitbucket.org/AGvin/gulp-framework.git
```

##Demo
See at https://bitbucket.org/AGvin/gulp-framework-demo