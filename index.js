module.exports = require('node-stdlib').Class.extend({
    settings: {
        plugins: {},
        paths: {
            root: './',
            tasks: 'gulp/tasks/',
            watches: 'gulp/watches/',
            events: 'gulp/events/'
        }
    },
    init: function (settings) {
        this.gulp = require('gulp');
        this._extendSettings(settings);

        require('app-module-path').addPath(this.settings.paths.root);
    },
    _extendSettings: function (settings) {
        var extend = require('node.extend'),
                settings = settings || {};

        //fix, for correct work of plugins, they will be skipped from extending
        if ('plugins' in settings) {
            var plugins = settings.plugins;
            delete settings.plugins;

            this.settings = extend(true, this.settings, settings);

            this.settings.plugins = plugins;
        } else {
            this.settings = extend(true, this.settings, settings);
        }
    },
    getGulpInst: function () {
        return this.gulp;
    },
    getGulpPlugins: function () {
        return this.settings.plugins;
    },
    getTask: function (taskName) {
        return require((this.settings.paths.tasks + taskName).replace(':', '/'))(this);
    },
    registerTask: function (taskName, executeAfterTasks) {
        var task = this.getTask(taskName),
                executeAfterTasks = executeAfterTasks || [];
        this.gulp.task(taskName, executeAfterTasks, task);
        return this;
    },
    getWatch: function (watchName) {
        return require((this.settings.paths.watches + watchName).replace(':', '/'))(this);
    },
    registerWatch: function (watchName) {
        require(this.settings.paths.events + 'watch')(
                this.getWatch(watchName)
                );
        return this;
    }
});